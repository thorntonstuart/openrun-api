<?php

namespace App\Observers;

use App\Models\Attendee;
use App\Notifications\EventQuotaReached;

class AttendeeObserver
{
    public function created(Attendee $attendee)
    {
        if ($attendee->event->attendeesEqualToQuota()) {
            $attendee->event->host->notify(
                new EventQuotaReached($attendee->event)
            );
        }
    }
}
