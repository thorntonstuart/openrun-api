<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class AlphaNumSpaces implements Rule
{
    /**
     * Regex to check against, by default it asserts that only
     * Numbers, Letters and Spaces are allowed
     *
     * @var string
     */
    protected $regex = '/^[A-Za-z0-9_., ~\-!@#\$%\^&*\(\)\p{L}]+$/iu';
    protected $message = 'alpha_num_spaces';

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return boolval(preg_match($this->regex, $value));
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans("validation.custom.{$this->message}");
    }

    /**
     * Validate against Numbers, Letters and Spaces
     *
     * @return $this
     */
    public static function lettersNumbersAndSpaces()
    {
        return (new static)->setRegex('/^[A-Za-z0-9_., ~\-!@#\$%\^&*\(\)\p{L}]+$/iu');
    }

    /**
     * Validate against Letters and Spaces
     *
     * @return $this
     */
    public static function lettersAndSpaces()
    {
        return (new static)
            ->setMessage('alpha_spaces')
            ->setRegex('/^[A-Za-z_., ~\-!@#\$%\^&*\(\)\p{L}]+$/iu');
    }

    /**
     * set the regex that the validator checks against
     *
     * @param string $regex
     * @return AlphaNumSpaces
     */
    public function setRegex(string $regex)
    {
        $this->regex = $regex;

        return $this;
    }

    /**
     * Set the path to the trans module that contains the message
     *
     * @param $transPath
     * @return $this
     */
    public function setMessage($transPath)
    {
        $this->message = $transPath;

        return $this;
    }
}