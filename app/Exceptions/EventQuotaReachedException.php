<?php

namespace App\Exceptions;

use Illuminate\Http\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class EventQuotaReachedException extends HttpException
{
    /**
     * The status code thrown with the exception.
     *
     * @var int
     */
    protected $statusCode = Response::HTTP_FORBIDDEN;

    /**
     * Exception message for fully booked event.
     *
     * @var string
     */
    protected $message = 'This event is now fully booked.';

    public function __construct(\Exception $previous = null, array $headers = [], ?int $code = 0)
    {
        parent::__construct(
            $this->statusCode,
            $this->message,
            $previous,
            $headers,
            $code
        );
    }
}