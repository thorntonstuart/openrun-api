<?php

namespace App\Models;

use App\Models\Attendee;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $guarded = [];

    protected $casts = [
        'start_datetime' => 'datetime',
        'end_datetime' => 'datetime',
    ];

    protected $dates = [
        'start_datetime',
        'end_datetime',
    ];

    public function address()
    {
        return $this->morphOne(Address::class, 'addressable');
    }

    public function host()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function attendees()
    {
        return $this->belongsToMany(User::class, 'attendees', 'event_id', 'user_id')
            ->using(Attendee::class)
            ->withTimestamps();
    }

    public function attendeesEqualToQuota()
    {
        return $this->attendees->count() === $this->quota;
    }

    public function attendeeQuotaReached()
    {
        return $this->attendees->count() > $this->quota;
    }
}
