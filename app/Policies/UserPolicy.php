<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Check that only authenticated user can update this user.
     *
     * @param User $authedUser
     * @param User $updatedUser
     * @return mixed
     */
    public function update(User $authedUser, User $updatedUser)
    {
        return $authedUser->is($updatedUser);
    }
}
