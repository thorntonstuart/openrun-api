<?php

namespace App\Http\Resources;

use App\Http\Resources\AddressResource;
use App\Http\Resources\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class EventResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'start_datetime' => $this->start_datetime->toDateTimeString(),
            'end_datetime' => $this->end_datetime->toDateTimeString(),
            'address' => AddressResource::make($this->whenLoaded('address')),
            'host' => UserResource::make($this->whenLoaded('host')),
            'attendees' => UserResource::collection($this->whenLoaded('attendees')),
            'created_at' => $this->created_at->toDateTimeString(),
        ];
    }
}
