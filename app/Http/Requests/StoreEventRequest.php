<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreEventRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'start_datetime' => [
                'required',
                'date_format:Y-m-d H:i:s',
                'after:now',
            ],
            'end_datetime' => [
                'required',
                'date_format:Y-m-d H:i:s',
                'after:start_datetime',
            ],
        ];
    }

    /**
     * Return event specific fields from request
     *
     * @return array
     */
    public function getEventFields()
    {
        return [
            'title' => $this->get('title'),
            'description' => $this->get('description'),
            'start_datetime' => $this->get('start_datetime'),
            'end_datetime' => $this->get('end_datetime'),
        ];
    }

    /**
     * Return event address fields from request
     *
     * @return array
     */
    public function getEventAddressFields()
    {
        return [
            'address_line_1' => $this->get('address_line_1'),
            'address_line_2' => $this->get('address_line_2'),
            'address_line_3' => $this->get('address_line_3'),
            'city' => $this->get('city'),
            'county' => $this->get('county'),
            'postal_code' => $this->get('postal_code'),
            'latitude' => $this->get('latitude'),
            'longitude' => $this->get('longitude'),
        ];
    }
}
