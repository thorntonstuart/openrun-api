<?php

namespace App\Http\Requests;

use App\Rules\AlphaNumSpaces;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class RegistrationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => [
                'required',
                'string',
                'min:2',
                'max:255',
                AlphaNumSpaces::lettersAndSpaces()
            ],
            'last_name' => [
                'required',
                'string',
                'min:2',
                'max:255',
                AlphaNumSpaces::lettersAndSpaces()
            ],
            'email' => [
                'required',
                'unique:users,email',
                'email',
                'max:255',
            ],
            'password' => [
                'required',
                'confirmed',
                'string',
                'min:8',
            ],
            'password_confirmation' => [
                'required',
                'min:8',
            ],
        ];
    }

    public function messages()
    {
        return [
            'first_name' => 'First name field is required',
            'last_name' => 'Last name field is required',
            'password:confirmed' => 'Password not confirmed',
        ];
    }

    public function getFields()
    {
        return [
            'first_name' => $this->input('first_name'),
            'last_name' => $this->input('last_name'),
            'email' => $this->input('email'),
            'password' => bcrypt($this->input('password')),
        ];
    }
}
