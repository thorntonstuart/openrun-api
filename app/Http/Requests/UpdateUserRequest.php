<?php

namespace App\Http\Requests;

use App\Rules\AlphaNumSpaces;
use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => [
                'string',
                'min:2',
                'max:255',
                AlphaNumSpaces::lettersAndSpaces()
            ],
            'last_name' => [
                'string',
                'min:2',
                'max:255',
                AlphaNumSpaces::lettersAndSpaces()
            ],
            'email' => [
                'unique:users,email',
                'email',
                'max:255',
            ],
            'allow_host_notifications' => [
                'boolean',
            ],
            'allow_promotional_communications' => [
                'boolean',
            ],
        ];
    }

    public function userFields()
    {
        return [
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'email' => $this->email,
        ];
    }

    public function profileFields()
    {
        return [
            'allow_host_notifications' => $this->allow_host_notifications,
            'allow_promotional_communications' => $this->allow_promotional_communications,
        ];
    }
}
