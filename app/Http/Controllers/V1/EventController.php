<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\V1\Controller;
use App\Http\Requests\StoreEventRequest;
use App\Http\Requests\UpdateEventRequest;
use App\Http\Resources\EventResource;
use App\Models\Address;
use App\Models\Event;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class EventController extends Controller
{
    protected $eventRelations = [
        'host',
        'attendees',
        'address',
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $events = Event::with($this->eventRelations)
            ->paginate(config('pagination.per_page'));
        
        return EventResource::collection($events);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreEventRequest  $request
     * @return EventResource
     */
    public function store(StoreEventRequest $request)
    {
        $event = $request->user()
            ->hostingEvents()
            ->create($request->getEventFields());

        $event->address()->create($request->getEventAddressFields());

        return (new EventResource($event->load('address', 'host')))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED, 'Event created');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Event  $event
     * @return EventResource
     */
    public function show(Event $event)
    {
        return new EventResource($event->load(...$this->eventRelations));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateEventRequest $request, Event $event)
    {
        $this->authorize('update', $event);

        $event->update($request->getEventFields());
        $event->address()->update($request->getEventAddressFields());

        return response('Event updated successfully', Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event)
    {
        $this->authorize('delete', $event);
        
        $event->delete();

        return response('Event deleted', Response::HTTP_NO_CONTENT);
    }
}
