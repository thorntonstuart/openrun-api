<?php

namespace App\Http\Controllers\V1;

use App\Exceptions\EventQuotaReachedException;
use App\Http\Controllers\V1\Controller;
use App\Http\Resources\EventResource;
use App\Models\Event;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AttendEventController extends Controller
{
    /**
     * Attach the user to the event that they intent to attend.
     *
     * @param Request $request
     * @param Event $event
     * @return EventResource
     */
    public function store(Request $request, Event $event)
    {
        throw_if($event->attendeeQuotaReached(), EventQuotaReachedException::class);
        
        $request->user()->attendingEvents()->attach($event->id);

        $event->load('attendees');

        return new EventResource($event);
    }
}
