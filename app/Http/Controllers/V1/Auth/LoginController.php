<?php

namespace App\Http\Controllers\V1\Auth;

use App\Http\Controllers\V1\Controller;
use App\Http\Resources\UserResource;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\UnauthorizedException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Login method for API
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function login(Request $request)
    {
        if (auth()->attempt($request->only('email', 'password'))) {
            return response()->json([
                'accessToken' => $this->guard()->user()->createToken('openrun-api')->accessToken
            ]);
        }

        return response()->json([
            'errors' => 'Unauthorised'
        ], Response::HTTP_UNAUTHORIZED);
    }
}