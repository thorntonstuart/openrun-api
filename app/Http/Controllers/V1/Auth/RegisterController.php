<?php

namespace App\Http\Controllers\V1\Auth;

use App\Models\Profile;
use App\Models\User;
use App\Http\Controllers\V1\Controller;
use App\Http\Requests\RegistrationRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Response;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  RegistrationRequest  $request
     * @return Response
     */
    protected function store(RegistrationRequest $request)
    {
        $user = User::create($request->getFields());
        $user->profile()->create();

        return response()->json([
            'message' => 'success',
        ], Response::HTTP_CREATED);
    }
}
