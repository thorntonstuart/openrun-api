<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\V1\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\UserResource;

class AuthUserAvatarController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return App\Http\Resources\UserResource
     */
    public function store(Request $request)
    {
        $request->user()->clearMediaCollection('avatar');
        $request->user()
            ->addMedia($request->file('avatar'))
            ->toMediaCollection('avatar');

        return new UserResource($request->user()->load('media'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return App\Http\Resources\UserResource
     */
    public function destroy(Request $request)
    {
        $request->user()->clearMediaCollection('avatar');
        
        return new UserResource($request->user()->load('media'));
    }
}
