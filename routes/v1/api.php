<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$router->as('auth.')->namespace('Auth')->group(function ($router) {
    $router->post('login', 'LoginController@login')->name('login');
    $router->apiResource('register', 'RegisterController')->only('store');
});

$router->get('events', 'EventController@index')->name('events.index');
$router->get('events/{event}', 'EventController@show')->name('events.show');

$router->middleware('auth:api')->group(function ($router) {
    $router->as('auth.')->namespace('Auth')->group(function ($router) {
        $router->delete('logout', 'LogoutController@destroy');
    });
    
    $router->post('events', 'EventController@store')->name('events.store');
    $router->patch('events/{event}', 'EventController@update')->name('events.update');
    $router->delete('events/{event}', 'EventController@destroy')->name('events.destroy');
    
    $router->post('events/{event}/attend', 'AttendEventController@store')->name('attend-event.store');

    $router->patch('users/{user}', 'UserController@update')->name('users.update');

    $router->group([
        'prefix' => 'me',
        'as' => 'me.',
    ], function ($router) {
        $router->get('/', 'AuthUserController@show');
        $router->post('avatar', 'AuthUserAvatarController@store')->name('avatar.store');
        $router->delete('avatar', 'AuthUserAvatarController@destroy')->name('avatar.delete');
    });
});
