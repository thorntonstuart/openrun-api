<?php

namespace Tests\Feature;

use App\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;

class RegisterUserTest extends TestCase
{
    use RefreshDatabase;
    
    public function test_a_user_can_register()
    {
        $this->json('POST', route('auth.register.store'), [
            'first_name' => 'Stuart',
            'last_name' => 'Thornton',
            'email' => 'testemail@test.com',
            'password' => 'testing123',
            'password_confirmation' => 'testing123',
        ])->assertStatus(Response::HTTP_CREATED);

        $this->assertDatabaseHas('users', [
            'first_name' => 'Stuart',
            'last_name' => 'Thornton',
            'email' => 'testemail@test.com',
            'deleted_at' => null,
        ]);
    }

    public function test_user_password_must_be_confirmed()
    {
        $this->expectException(ValidationException::class);

        $this->json('POST', route('auth.register.store'), [
            'first_name' => 'Stuart',
            'last_name' => 'Thornton',
            'email' => 'testemail@test.com',
            'password' => 'testing123',
            'password_confirmation' => '',
        ]);
    }

    public function test_user_has_profile_on_creation()
    {
        $this->json('POST', route('auth.register.store'), [
            'first_name' => 'Stuart',
            'last_name' => 'Thornton',
            'email' => 'testemail@test.com',
            'password' => 'testing123',
            'password_confirmation' => 'testing123',
        ])->assertStatus(Response::HTTP_CREATED);

        $user = User::whereEmail('testemail@test.com')->first();

        $this->assertDatabaseHas('profiles', [
            'user_id' => $user->id,
        ]);
    }
}
