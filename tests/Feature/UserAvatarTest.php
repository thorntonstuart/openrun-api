<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Illuminate\Http\Testing\File;
use Tests\TestCase;

class UserAvatarTest extends TestCase
{
    use RefreshDatabase;

    public $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = factory(User::class)->create();

        config()->set('filesystems.disks.media', [
            'driver' => 'local',
            'root' => __DIR__ . '/../../temp',
        ]);
        config()->set('medialibrary.default_filesystem', 'media');
    }

    public function test_a_user_can_upload_an_avatar()
    {
        $response = $this->actingAs($this->user, 'api')
            ->json('POST', route('me.avatar.store'), [
                'avatar' => File::image('avatar.jpg'),
            ])
            ->assertStatus(Response::HTTP_OK)
            ->assertSee('avatar.jpg')
            ->assertJsonStructure([
                'data' => [
                    'avatar_url',
                ],
            ]);

        $avatar = $this->user->getMedia('avatar');
        $this->assertFileExists($avatar->first()->getPath());
    }

    public function test_a_user_can_change_their_existing_avatar()
    {
        $avatarOneResponse = $this->actingAs($this->user, 'api')
            ->json('POST', route('me.avatar.store'), [
                'avatar' => File::image('avatar-one.jpg'),
            ])
            ->assertStatus(Response::HTTP_OK)
            ->assertSee('avatar-one.jpg');

        $avatarTwoResponse = $this->actingAs($this->user, 'api')
            ->json('POST', route('me.avatar.store'), [
                'avatar' => File::image('avatar-two.jpg'),
            ])
            ->assertStatus(Response::HTTP_OK)
            ->assertSee('avatar-two.jpg')
            ->assertJsonStructure([
                'data' => [
                    'avatar_url',
                ],
            ]);

        $avatar = $this->user->getMedia('avatar');
        $this->assertFileExists($avatar->first()->getPath());
        $this->assertEquals('avatar-two.jpg', $avatar->first()->file_name);
    }

    public function test_a_user_can_remove_their_avatar()
    {
        $this->actingAs($this->user, 'api')
            ->json('POST', route('me.avatar.store'), [
                'avatar' => File::image('avatar-one.jpg'),
            ])
            ->assertStatus(Response::HTTP_OK)
            ->assertSee('avatar-one.jpg')
            ->assertJsonStructure([
                'data' => [
                    'avatar_url',
                ],
            ]);

        $this->delete(route('me.avatar.delete'))
            ->assertStatus(Response::HTTP_OK);

        $this->assertEmpty($this->user->getMedia('avatar')->toArray());
    }

    public function test_a_user_can_only_have_one_avatar()
    {
        $this->actingAs($this->user, 'api')
            ->json('POST', route('me.avatar.store'), [
                'avatar' => File::image('avatar-one.jpg'),
            ]);

        $this->actingAs($this->user, 'api')
            ->json('POST', route('me.avatar.store'), [
                'avatar' => File::image('avatar-two.jpg'),
            ])
            ->assertStatus(Response::HTTP_OK)
            ->assertSee('avatar-two.jpg')
            ->assertJsonStructure([
                'data' => [
                    'avatar_url',
                ],
            ]);
        
        $avatar = $this->user->getMedia('avatar');
        $this->assertCount(1, $avatar->toArray());
    }
}
