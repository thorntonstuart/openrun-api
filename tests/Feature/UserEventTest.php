<?php

namespace Tests\Feature;

use App\Exceptions\EventQuotaReachedException;
use App\Models\Address;
use App\Models\Event;
use App\Models\User;
use Illuminate\Auth\Access\AuthorizationException;
use Tests\TestCase;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;

class UserEventTest extends TestCase
{
    use RefreshDatabase;

    public $user;
    public $event;
    public $address;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = factory(User::class)->create();
        $this->event = factory(Event::class)->make();
        $this->address = factory(Address::class)->make();
    }

    public function test_any_user_can_see_a_list_of_events()
    {
        $events = factory(Event::class, 10)->create([
            'user_id' => $this->user->id,
        ]);
        
        $firstEvent = $events->first();
        $attendee = tap(factory(User::class)->create(), function ($user) use ($firstEvent) {
            $user->attendingEvents()->attach($firstEvent->id);
        });

        $response = $this->json('GET', route('events.index'));
        $response->assertStatus(Response::HTTP_OK)
            ->assertJsonFragment([
                'id' => $firstEvent->id,
                'title' => $firstEvent->title,
                'description' => $firstEvent->description,
                'start_datetime' => $firstEvent->start_datetime->toDateTimeString(),
                'end_datetime' => $firstEvent->end_datetime->toDateTimeString(),
                'address' => null,
                'host' => [
                    'id' => $this->user->id,
                    'first_name' => $this->user->first_name,
                    'last_name' => $this->user->last_name,
                    'email' => $this->user->email,
                    'email_verified_at' => $this->user->email_verified_at->toDateTimeString(),
                    'avatar_url' => null,
                ],
                'attendees' => [
                    [
                        'id' => $attendee->id,
                        'first_name' => $attendee->first_name,
                        'last_name' => $attendee->last_name,
                        'email' => $attendee->email,
                        'email_verified_at' => $attendee->email_verified_at->toDateTimeString(),
                        'avatar_url' => null,
                    ]
                ],
                'created_at' => $firstEvent->created_at->toDateTimeString(),
            ])
            ->assertJsonCount(10, 'data');
    }

    public function test_any_user_can_see_a_single_event()
    {
        $event = factory(Event::class)->create([
            'title' => $this->event->title,
            'description' => $this->event->description,
            'start_datetime' => $this->event->start_datetime->format('Y-m-d H:i:s'),
            'end_datetime' => $this->event->end_datetime->format('Y-m-d H:i:s'),
            'user_id' => $this->user->id,
        ]);

        $attendee = factory(User::class)->create();

        $event->attendees()->attach($attendee);
        $event->address()->create([
            'address_line_1' => $this->address->address_line_1,
            'address_line_2' => $this->address->address_line_2,
            'address_line_3' => $this->address->address_line_3,
            'city' => $this->address->city,
            'county' => $this->address->county,
            'postal_code' => $this->address->postal_code,
            'country' => $this->address->country,
            'longitude' => $this->address->longitude,
            'latitude' => $this->address->latitude,
        ]);

        $this->json('GET', route('events.show', [
            'event' => $event->id,
        ]))
        ->assertStatus(Response::HTTP_OK)
        ->assertJson([
            'data' => [
                'id' => $event->id,
                'title' => $event->title,
                'description' => $event->description,
                'start_datetime' => $event->start_datetime->toDateTimeString(),
                'end_datetime' => $event->end_datetime->toDateTimeString(),
                'address' => [
                    'address_line_1' => $this->address->address_line_1,
                    'address_line_2' => $this->address->address_line_2,
                    'address_line_3' => $this->address->address_line_3,
                    'city' => $this->address->city,
                    'county' => $this->address->county,
                    'postal_code' => $this->address->postal_code,
                    'country' => $this->address->country,
                    'latitude' => $this->address->latitude,
                    'longitude' => $this->address->longitude,
                ],
                'host' => [
                    'id' => $this->user->id,
                    'first_name' => $this->user->first_name,
                    'last_name' => $this->user->last_name,
                    'email' => $this->user->email,
                    'email_verified_at' => $this->user->email_verified_at->toDateTimeString(),
                    'avatar_url' => null,
                ],
                'attendees' => [
                    [
                        'id' => $attendee->id,
                        'first_name' => $attendee->first_name,
                        'last_name' => $attendee->last_name,
                        'email' => $attendee->email,
                        'email_verified_at' => $attendee->email_verified_at->toDateTimeString(),
                        'avatar_url' => null,
                    ]
                ],
                'created_at' => $event->created_at->toDateTimeString(),
            ]
        ]);
    }

    public function test_a_logged_in_user_can_create_an_event()
    {
        $this->actingAs($this->user, 'api')
            ->json('POST', route('events.store'), [
                'title' => $this->event->title,
                'description' => $this->event->description,
                'start_datetime' => $this->event->start_datetime->format('Y-m-d H:i:s'),
                'end_datetime' => $this->event->end_datetime->format('Y-m-d H:i:s'),
                'address_line_1' => $this->address->address_line_1,
                'address_line_2' => $this->address->address_line_2,
                'address_line_3' => $this->address->address_line_3,
                'city' => $this->address->city,
                'postal_code' => $this->address->postal_code,
                'country' => $this->address->country,
                'longitude' => $this->address->longitude,
                'latitude' => $this->address->latitude,
            ])
            ->assertStatus(Response::HTTP_CREATED);

        $this->assertDatabaseHas('events', [
            'title' => $this->event->title,
            'user_id' => $this->user->id,
        ]);
        $this->assertDatabaseHas('addresses', [
            'address_line_1' => $this->address->address_line_1,
            'addressable_type' => 'events',
        ]);
    }

    public function test_a_non_authed_user_cannot_create_an_event()
    {
        $this->expectException(AuthenticationException::class);

        $this->json('POST', route('events.store'), [
            'title' => $this->event->title,
            'description' => $this->event->description,
            'start_datetime' => $this->event->start_datetime,
            'end_datetime' => $this->event->end_datetime,
            'address_line_1' => $this->address->address_line_1,
            'address_line_2' => $this->address->address_line_2,
            'address_line_3' => $this->address->address_line_3,
            'city' => $this->address->city,
            'postal_code' => $this->address->postal_code,
            'country' => $this->address->country,
            'longitude' => $this->address->longitude,
            'latitude' => $this->address->latitude,
        ])
        ->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function test_a_logged_in_user_can_attend_an_event()
    {
        $attendee = factory(User::class)->create();
        $event = factory(Event::class)->create([
            'user_id' => $this->user->id,
            'quota' => 20,
        ]);

        $this->actingAs($attendee, 'api')
            ->json('POST', route('attend-event.store', [
                'event_id' => $event->id,
            ]))
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonFragment([
                'attendees' => [
                    [
                        'id' => $attendee->id,
                        'first_name' => $attendee->first_name,
                        'last_name' => $attendee->last_name,
                        'email' => $attendee->email,
                        'email_verified_at' => $attendee->email_verified_at->toDateTimeString(),
                        'avatar_url' => null,
                    ]
                ]
            ]);
            
        $this->assertDatabaseHas('attendees', [
            'user_id' => $attendee->id,
            'event_id' => $event->id,
        ]);
    }

    public function a_guest_user_cannot_attend_an_event()
    {
        $this->expectException(AuthenticationException::class);

        $event = factory(Event::class)->create([
            'user_id' => $this->user->id,
        ]);

        $this->json('POST', route('attend-event.store', [
            'event' => $event->id,
        ]));
    }

    public function test_a_logged_in_user_can_update_their_own_event()
    {
        $event = factory(Event::class)->create([
            'user_id' => $this->user->id,
        ]);

        $this->actingAs($this->user, 'api')
            ->json('PATCH', route('events.update', ['event' => $event->id]), [
                'title' => 'This is a test',
            ])
            ->assertSuccessful()
            ->assertSee('Event updated successfully');
        }

    public function test_a_logged_in_user_cannot_update_another_users_event()
    {
        $this->expectException(AuthorizationException::class);
        
        $unauthorizedUser = factory(User::class)->create();
        $event = factory(Event::class)->create([
            'user_id' => $this->user->id,
        ]);

        $this->actingAs($unauthorizedUser, 'api')
            ->json('PATCH', route('events.update', ['event' => $event->id]), [
                'title' => 'This is a test',
            ])
            ->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function test_a_logged_in_user_can_delete_their_own_event()
    {
        $event = factory(Event::class)->create([
            'user_id' => $this->user->id,
        ]);
        
        $this->actingAs($this->user, 'api')
            ->delete(route('events.destroy', [
                'event' => $event->id,
            ]));

        $this->assertDatabaseMissing('events', [
            'title' => $event->title,
        ]);
    }

    public function test_a_logged_in_user_cannot_delete_another_users_event()
    {
        $this->expectException(AuthorizationException::class);

        $event = factory(Event::class)->create([
            'user_id' => $this->user->id,
        ]);

        $secondUser = factory(User::class)->create();
        $this->actingAs($secondUser, 'api')
            ->delete(route('events.destroy', [
                'event' => $event->id,
            ]))
            ->assertStatus(Response::HTTP_FORBIDDEN);
    }

    public function test_event_attendees_count_cannot_exceed_attendees_quota()
    {
        $this->expectException(EventQuotaReachedException::class);

        $attendees = factory(User::class, 20)->create();
        $testUser = factory(User::class)->create();
        $event = factory(Event::class)->create([
            'user_id' => $this->user->id,
        ]);

        $event->attendees()->attach($attendees);
        
        $this->actingAs($testUser, 'api')
            ->json('POST', route('attend-event.store', [
                'event' => $event->id,
            ]))
            ->assertStatus(Response::HTTP_FORBIDDEN)
            ->assertSee('This event is now fully booked');
    }
}
