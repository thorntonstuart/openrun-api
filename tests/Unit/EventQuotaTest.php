<?php

namespace Tests\Unit;

use App\Models\Event;
use App\Models\User;
use App\Notifications\EventQuotaReached;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Notification;

class EventQuotaTest extends TestCase
{
    public function setUp() : void
    {
        parent::setUp();
        $this->user = factory(User::class)->create();
    }

    public function test_notification_is_sent_when_event_quota_reached()
    {
        Notification::fake();

        $attendees = factory(User::class, 19)->create();
        $testUser = factory(User::class)->create();

        $event = factory(Event::class)->create([
            'user_id' => $this->user->id,
            'quota' => 20,
        ]);

        $event->attendees()->attach($attendees);

        $this->actingAs($testUser, 'api')
            ->json('POST', route('attend-event.store', [
                'event' => $event->id,
            ]));
        
        Notification::assertSentTo(
            $this->user,
            EventQuotaReached::class
        );
    }
}
