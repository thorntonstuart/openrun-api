<?php

namespace Tests\Unit;

use App\Models\User;
use Tests\TestCase;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;

class UpdateUserTest extends TestCase
{
    use RefreshDatabase;

    public $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = factory(User::class)->create();
        $this->user->profile()->create();
    }

    public function test_authenticated_user_can_update_their_own_user()
    {
        $response = $this->actingAs($this->user, 'api')
            ->json('PATCH', route('users.update', $this->user->id), [
                'first_name' => 'Testing',
                'last_name' => 'User',
                'email' => 'testinguser@email.com',
                'allow_host_notifications' => true,
                'allow_promotional_communications' => false,
            ])
            ->assertStatus(Response::HTTP_OK);
        
        $this->assertDatabaseHas('users', [
            'id' => $this->user->id,
            'first_name' => 'Testing',
            'last_name' => 'User',
        ]);

        $this->assertDatabaseHas('profiles', [
            'user_id' => $this->user->id,
            'allow_host_notifications' => true,
            'allow_promotional_communications' => false,
        ]);
    }

    public function test_non_authenticated_user_cannot_update_a_user()
    {
        $this->expectException(AuthenticationException::class);

        $this->json('PATCH', route('users.update', $this->user->id), [
            'first_name' => 'Testing',
            'last_name' => 'User',
            'email' => 'testinguser@email.com',
            'allow_host_notifications' => true,
            'allow_promotional_communications' => false,
        ]);
    }

    public function test_authenticated_user_cannot_update_another_user()
    {
        $this->expectException(AuthorizationException::class);

        $intruder = factory(User::class)->create();

        $this->actingAs($intruder, 'api')
            ->json('PATCH', route('users.update', $this->user->id), [
                'first_name' => 'Testing',
                'last_name' => 'User',
                'email' => 'testinguser@email.com',
                'allow_host_notifications' => true,
                'allow_promotional_communications' => false,
            ]);
    }

    public function test_user_cannot_enter_invalid_email()
    {
        $this->expectException(ValidationException::class);

        $response = $this->actingAs($this->user, 'api')
            ->json('PATCH', route('users.update', $this->user->id), [
                'first_name' => 'Testing',
                'last_name' => 'User',
                'email' => 'fasjkfa;fsajklasfjk',
                'allow_host_notifications' => true,
                'allow_promotional_communications' => false,
            ])
            ->assertStatus(Response::HTTP_OK);
    }
}
