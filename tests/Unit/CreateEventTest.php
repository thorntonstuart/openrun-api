<?php

namespace Tests\Unit;

use App\Models\User;
use App\Models\Event;
use App\Models\Address;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Tests\TestCase;

class CreateEventTest extends TestCase
{
    public $user;

    public function setUp() : void
    {
        parent::setUp();
        $this->user = factory(User::class)->create();
        $this->event = factory(Event::class)->make();
        $this->address = factory(Address::class)->make();
    }

    public function test_event_dates_in_correct_format_pass_validation()
    {
        $this->actingAs($this->user, 'api')
            ->json('POST', route('events.store'), [
                'title' => $this->event->title,
                'description' => $this->event->description,
                'start_datetime' => Carbon::parse($this->event->start_datetime)->format('Y-m-d H:i:s'),
                'end_datetime' => Carbon::parse($this->event->end_datetime)->format('Y-m-d H:i:s'),
                'address_line_1' => $this->address->address_line_1,
                'address_line_2' => $this->address->address_line_2,
                'address_line_3' => $this->address->address_line_3,
                'city' => $this->address->city,
                'postal_code' => $this->address->postal_code,
                'country' => $this->address->country,
                'longitude' => $this->address->longitude,
                'latitude' => $this->address->latitude,
            ])
            ->assertStatus(Response::HTTP_CREATED);
    }

    public function test_event_dates_in_incorrect_format_do_not_pass_validation()
    {
        $this->expectException(ValidationException::class);

        $this->actingAs($this->user, 'api')
            ->json('POST', route('events.store'), [
                'title' => $this->event->title,
                'description' => $this->event->description,
                'start_datetime' => $this->event->start_datetime,
                'end_datetime' => $this->event->end_datetime,
                'address_line_1' => $this->address->address_line_1,
                'address_line_2' => $this->address->address_line_2,
                'address_line_3' => $this->address->address_line_3,
                'city' => $this->address->city,
                'postal_code' => $this->address->postal_code,
                'country' => $this->address->country,
                'longitude' => $this->address->longitude,
                'latitude' => $this->address->latitude,
            ])
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function test_event_start_datetime_cannot_be_before_end_datetime()
    {
        $this->expectException(ValidationException::class);

        $this->actingAs($this->user, 'api')
            ->json('POST', route('events.store'), [
                'title' => $this->event->title,
                'description' => $this->event->description,
                'start_datetime' => Carbon::parse($this->event->start_datetime)->format('Y-m-d H:i:s'),
                'end_datetime' => Carbon::parse($this->event->start_datetime->subHours(4))->format('Y-m-d H:i:s'),
                'address_line_1' => $this->address->address_line_1,
                'address_line_2' => $this->address->address_line_2,
                'address_line_3' => $this->address->address_line_3,
                'city' => $this->address->city,
                'postal_code' => $this->address->postal_code,
                'country' => $this->address->country,
                'longitude' => $this->address->longitude,
                'latitude' => $this->address->latitude,
            ])
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function test_event_cannot_start_in_the_past()
    {
        $this->expectException(ValidationException::class);

        $this->actingAs($this->user, 'api')
            ->json('POST', route('events.store'), [
                'title' => $this->event->title,
                'description' => $this->event->description,
                'start_datetime' => now()->subHours(8)->format('Y-m-d H:i:s'),
                'end_datetime' => Carbon::parse($this->event->end_datetime)->format('Y-m-d H:i:s'),
                'address_line_1' => $this->address->address_line_1,
                'address_line_2' => $this->address->address_line_2,
                'address_line_3' => $this->address->address_line_3,
                'city' => $this->address->city,
                'postal_code' => $this->address->postal_code,
                'country' => $this->address->country,
                'longitude' => $this->address->longitude,
                'latitude' => $this->address->latitude,
            ])
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
